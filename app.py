# -*- coding: utf-8 -*-
import re
import urllib.request
import requests

from bs4 import BeautifulSoup

from flask import Flask
from slack import WebClient
from slackeventsapi import SlackEventAdapter

from slack.web.classes import extract_json
from slack.web.classes.blocks import *
from slack.web.classes.elements import *
from slack.web.classes.interactions import MessageInteractiveEvent
from slackeventsapi import SlackEventAdapter

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'}

SLACK_TOKEN = 'xoxb-691801693830-678259008899-I1yxH3fkd6glbOGXRyJFw0gG'
SLACK_SIGNING_SECRET = '1202d32b060c2b6a8c59be2b5abe51f0'

app = Flask(__name__)
# /listening 으로 슬랙 이벤트를 받습니다.
slack_events_adaptor = SlackEventAdapter(SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=SLACK_TOKEN)

SELECT_SWITCH = 0

def greetings():

    button_actions = ActionsBlock(
        block_id=text,
        elements=[
            ButtonElement(
                text="가수",
                action_id="button1", value=str('제목')
            ),
            ButtonElement(
                text="제목",
                action_id="button2", value=str('가수')
            ),
            ButtonElement(
                text="가사",
                action_id="button3", value=str('가사')
            ),
        ]
    )
    return "가수, 제목, 가사 3개 주제 중 하나를 입력한 뒤\n주제와 연관되게 검색어를 입력 하시면 됩니다.\n정보의 출처는 멜론(https://www.melon.com/)입니다."


def __common_logic(select, text):
    url_list = ["total","song","lyric"]
    header = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'}
    search_keyword = text.split(' ', maxsplit=1)[1].replace("<", "").replace(">", "")
    url = "https://www.melon.com/search/" + url_list[int(select) - 1] + "/index.htm?q=" + search_keyword + "&section=&searchGnbYn=Y&kkoSpl=Y&kkoDpType=&linkOrText=T&ipath=srch_form"
    source_code = requests.get(url, headers=header).text
    soup = BeautifulSoup(source_code, "html.parser")
    return soup

def __search_lyric(soup):
    global SELECT_SWITCH
    SELECT_SWITCH = 0
    lyric_titles = []
    lyric_contents = []
    lyric_artists = []
    lyric_total = []
    for lyric_list in soup.find("div", class_="list_lyric").find_all("dl", class_="cntt_lyric"):
        for lyric_title in lyric_list.find_all("a", class_="text"):
            lyric_titles.append(lyric_title.get_text())
        for lyric_content in lyric_list.find_all("dd", class_="lyric"):
            lyric_contents.append(lyric_content.get_text())
        for lyric_artist in lyric_list.find_all("span", class_="atist"):
            # print(lyric_artist.get_text().replace("\n", " ").replace("\t",""))
            # print(r.findall(lyric_artist.get_text().replace("\n", " ").replace("\t","").replace("\r","")))
            lyric_artists.append(lyric_artist.get_text())
    for i in range(5):
        lyric_total.append(
            "*" + str(i + 1) + ". " + lyric_titles[i] + "*" + "\n" + "-" * 120 + lyric_contents[i].replace("~", "`~") + "-" * 120 + "\n" + "가수 : " +
            lyric_artists[i].replace("\n","").replace("\t","").replace("\r","") + "\n\n")

    return lyric_total

def __search_song_title(soup):
    global SELECT_SWITCH
    SELECT_SWITCH = 0
    song_titles = []
    song_artists = []
    for i, song_title in enumerate(
            soup.find("div", class_="tb_list d_song_list songTypeOne").find_all("a", class_="fc_gray")):
        if i >= 5:
            break
        song_titles.append(song_title.get_text())
    for j, song_artist in enumerate(
            soup.find("div", class_="tb_list d_song_list songTypeOne").find_all("a", class_="fc_mgray")):
        if j >= 5:
            break
        song_artists.append(song_artist.get_text())
    for k in range(len(song_titles)):
        song_titles[k] = str(k + 1) + ". " + song_artists[k] + " : " + song_titles[k]

    return song_titles

def __search_artist(soup):
    global SELECT_SWITCH
    SELECT_SWITCH = 0
    artist_intros = []
    artist_intro_contents = []

    for keyword in soup.find("div", class_="wrap_cntt clfix d_artist_list").find("dl", class_="info_02 clfix").find_all("dt"):
        artist_intros.append(keyword.get_text().strip())
    for keyword in soup.find("div", class_="wrap_cntt clfix d_artist_list").find("dl", class_="info_02 clfix").find_all("dd"):
        pre_processing = keyword.get_text().split('\n')
        processing = ''
        if len(pre_processing) == 1:
            artist_intro_contents.append(keyword.get_text())
        else:
            for value in pre_processing:
                if value != '':
                    if value != '곡재생':
                        if not processing:
                            processing = value
                        else:
                            processing += '(' + value + ')'
            artist_intro_contents.append(processing)

    image = soup.find("div", class_="wrap_cntt clfix d_artist_list").find("img")

    for i in range(len(artist_intros)):
        artist_intros[i] = artist_intros[i] + " : " + artist_intro_contents[i]

    # 키워드 리스트를 문자열로 만듭니다.
    return artist_intros, image

# 크롤링 함수 구현하기
def _crawl_naver_keywords(text):
    global SELECT_SWITCH

    if SELECT_SWITCH == 0:
        # 예외 처리
        # 멘션만 보낼 경우 아래에 작성된 split으로는 인덱스 접근 오류가 발생
        # 이를 예외 처리 하여 봇 사용 방법을 알려 주도록 함
        try:
            select_text = text.split(' ', maxsplit=1)[1].replace("<", "").replace(">", "")
        except IndexError:
            return greetings(), 0

        if select_text == "가수":
            SELECT_SWITCH = 1
            return "어떤 가수?", 0
        elif select_text == "제목":
            SELECT_SWITCH = 2
            return "어떤 제목?", 0
        elif select_text == "가사":
            SELECT_SWITCH = 3
            return "어떤 가사?", 0
        else:
            return greetings(), 0
    else:
        if SELECT_SWITCH == 3:
            # 리스트를 문자열 형태로 바꾸는 방식
            return '\n'.join(__search_lyric(__common_logic(SELECT_SWITCH, text))), 0
        elif SELECT_SWITCH == 1:
            # 리스트를 문자열 형태로 바꾸는 방식
            return __search_artist(__common_logic(SELECT_SWITCH, text))
        elif SELECT_SWITCH == 2:
            # 리스트를 문자열 형태로 바꾸는 방식
            return '\n'.join(__search_song_title(__common_logic(SELECT_SWITCH, text))), 0
# def _crawl_portal_keywords(text):
#     global COUNT
#
#     search_keyword = text.split(' ', maxsplit=1)[1].replace("<", "").replace(">", "")
#
#     # test = text.split(' ')[1:]
#     # print(test)
#     #
#     # url = "https://www.melon.com/search/total/index.htm?q="+test[0]+'+'+test[1]+"&section=&searchGnbYn=Y&kkoSpl=Y&kkoDpType=&linkOrText=T&ipath=srch_form"
#     # print(url)
#     # # url_match = re.search(r'<(http.*?)(\|.*?)?>', text)
#     # # if not url_match:
#     # #     return '올바른 URL을 입력해주세요.'
#     # # # print(url)
#     # # url = url_match.group(1)
#     # html = requests.get(url, headers=headers).text
#     # soup = BeautifulSoup(html, "html.parser")
#
#     url = 'https://www.melon.com/search/total/index.htm?q=' + search_keyword + '&section=&searchGnbYn=Y&kkoSpl=N&kkoDpType=&ipath=srch_form'
#     source_code = requests.get(url, headers=headers).text
#     soup = BeautifulSoup(source_code, "html.parser")
#
#     # pre_processing = ''
#     keywords1 = []
#     keywords2 = []
#
#     for keyword in soup.find("div", class_="wrap_cntt clfix d_artist_list").find("dl", class_="info_02 clfix").find_all(
#             "dt"):
#         keywords1.append(keyword.get_text().strip())
#     for keyword in soup.find("div", class_="wrap_cntt clfix d_artist_list").find("dl", class_="info_02 clfix").find_all(
#             "dd"):
#         pre_processing = keyword.get_text().split('\n')
#         processing = ''
#         if len(pre_processing) == 1:
#             keywords2.append(keyword.get_text())
#         else:
#             for value in pre_processing:
#                 if value != '':
#                     if value != '곡재생':
#                         if not processing:
#                             processing = value
#                         else:
#                             processing += '(' + value + ')'
#             keywords2.append(processing)
#
#     image=soup.find("div", class_="wrap_cntt clfix d_artist_list").find("img")
#     # img=image.get('src')
#
#     for i in range(len(keywords1)):
#         keywords1[i] = keywords1[i] + " : " + keywords2[i]
#     # ARTIST_NAME = 0
#     COUNT = True
#     # 키워드 리스트를 문자열로 만듭니다.
#     return '\n'.join(keywords1), image
#
#     # list_title, list_lyric = [], []
#     # for melon in soup.find("div", class_="list_lyric"):
#     #     melon.find_all("a", class_="text")
#     #     m_list.append(melon.getText())
#     #     melon.find_all("dd", class_="lyric")
#     #     m_list.append(melon.getText())
#     #     if not melon in m_list:
#     #         m_list.append(melon.getText())
#     #
#     # # 여기에 함수를 구현해봅시다.
#     # keywords = []
#     # if "naver" in url:
#     #     for data in (soup.find_all("span", class_="ah_k")):
#     #         if not data.get_text() in keywords:
#     #             if len(keywords) >= 10:
#     #                 break
#     #             keywords.append(data.get_text())
#     #
#     # elif "daum" in url:
#     #     for data in soup.find_all("a", class_="link_issue"):
#     #         if not data.get_text() in keywords:
#     #             keywords.append(data.get_text())
#     #
#     # elif "melon" in url:
#     #     print('******************************************')
#     #     for data in soup.find_all("p", class_="song"):
#     #         if not data.get_text() in keywords:
#     #             keywords.append(data.get_text())
#     #
#     # # 키워드 리스트를 문자열로 만듭니다.
#     # return u'\n'.join(m_list)

prev_time_stamp = 0
now_time_stamp = 0
# 챗봇이 멘션을 받았을 경우
@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):
    global prev_time_stamp
    global now_time_stamp
    channel = event_data["event"]["channel"]
    text = event_data["event"]["text"]

    if prev_time_stamp == 0:
        prev_time_stamp = event_data["event_time"]
        now_time_stamp = prev_time_stamp + 1

    else:
        now_time_stamp = event_data["event_time"]

    print(now_time_stamp)
    print("a")
    if prev_time_stamp < now_time_stamp:
        prev_time_stamp = now_time_stamp
        print(now_time_stamp)
        print("b")
    else:
        return 404, 'ERROR'

    keywords, img = _crawl_naver_keywords(text)

    if img != 0:
        block1 = SectionBlock(
            text='\n'.join(keywords)
        )

        block2 = ImageBlock(
            image_url=img.get('src'),
            alt_text="이미지가 안 보여요"
        )

        my_blocks = [block2, block1]
        slack_web_client.chat_postMessage(
            channel=channel,
            blocks=extract_json(my_blocks)
            # text=keywords
        )
    else:
        slack_web_client.chat_postMessage(
            channel=channel,
            text=keywords
            # text=keywords
        )
    text = ""
    # return 200, 'OK'

# / 로 접속하면 서버가 준비되었다고 알려줍니다.
@app.route("/", methods=["GET"])
def index():
    return "<h1>Server is ready.</h1>"


if __name__ == '__main__':
    app.run('127.0.0.1', port=5000)
